Description
===========
 Userpoints VBulliten integrates Drupal userpoints module with VBulliten forum
 When users add new posts on vb they get points on Drupal

Dependencies
============
 - drupalvb
 - userpoins

Installation
============
 The module automatically installs the require vbplugin ,

 You must visit AdminCP -> Plugins & Products -> Manage Products and Enable
 Drupal UserPoints product.
 Also whenever you update the module you should visit the same page again
 and update the product if there is any update availble

 You can simply visit admin/config/people/userpoints/settings
  Click on VBulliten tab and customize the parameters

Author and Maintainer
=====================
 Sina Salek 7.x (http://drupal.org/user/52244)